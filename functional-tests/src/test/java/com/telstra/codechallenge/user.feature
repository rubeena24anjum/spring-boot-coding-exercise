Feature: As a developer i want to test the users uri

  Scenario: Is the users uri available and functioning
    Given url microserviceUrl
    And path '/users'
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    And match response == 
    """
  [{"html_url":"https:\/\/github.com\/errfree","id":44,"login":"errfree"},{"html_url":"https:\/\/github.com\/engineyard","id":81,"login":"engineyard"}]
    And match response.login == "errfree"
    """
    
    