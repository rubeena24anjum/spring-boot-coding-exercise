package com.telstra.codechallenge.user;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class SpringBootUserController {
	
	 private SpringBootUserService springBootUserService;

	  public SpringBootUserController(
			  SpringBootUserService springBootUserService) {
	    this.springBootUserService = springBootUserService;
	  }
	 
	  /**
	   * "/users" api takes input numberOfAccountToReturn and return list of old user accounts with 0 follower 
	   */
	  @RequestMapping(path = "/users", method = RequestMethod.GET)
	  public List<ResponseUserDTO> getUser(@RequestParam(value = "numberOfAccountToReturn", defaultValue = "2") int numberOfAccountToReturn) {
	    return springBootUserService.getUser(numberOfAccountToReturn);
	  }

}
