package com.telstra.codechallenge.user;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class SpringBootUserService {
	
	/**
	 * userBaseUrl refers to "https://api.github.com/search/users?q=followers:0&sort=joined&order=asc" which returns oldest user accounts(when they joined order by asc)
	 * which have 0 follower 
	 */
	@Value("${user.base.url}")
	  private String userBaseUrl;

	  private RestTemplate restTemplate;

	  public SpringBootUserService(RestTemplate restTemplate) {
	    this.restTemplate = restTemplate;
	  }
	  
	  /**
	   * getUser method takes input numberOfAccountToReturn and return respective user accounts as List<ResponseUserDTO>. 
	   * ResponseUserDTO contains 3 fields id,login and html_url
	   */
	  public List<ResponseUserDTO> getUser(int numberOfAccountToReturn) {
		  List<ResponseUserDTO> listUserDTO = new ArrayList<ResponseUserDTO>(); 
		  
		  User forObject = restTemplate.getForObject(userBaseUrl, User.class);
		  if(forObject.getItems()!= null && numberOfAccountToReturn!=0) {
			  List<Item> items = forObject.getItems();
		     		     
			  for(int i=0; i<numberOfAccountToReturn; i++) {
		    	 ResponseUserDTO responseUserDTO = new ResponseUserDTO();
		    	 responseUserDTO.setId(items.get(i).getId());
		    	 responseUserDTO.setLogin(items.get(i).getLogin());
		    	 responseUserDTO.setHtml_url(items.get(i).getHtml_url());
		    	 
		    	 listUserDTO.add(responseUserDTO);		    		    	 
		     }	
		  }
			return listUserDTO;		     
		  }
}
